package com.example.persistence.repository;

import com.example.persistence.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.graphql.data.GraphQlRepository;

@GraphQlRepository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
}
