package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GraphqlRepositoryApplication {

    public static void main(String[] args) {
        SpringApplication.run(GraphqlRepositoryApplication.class, args);
    }

}
