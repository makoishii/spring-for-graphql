# spring-for-graphql

JSUG 勉強会 JSUG勉強会2024その1 Spring for GraphQL入門パートのサンプルコードです。

## ライセンス

LGPL

個人学習目的のサンプルコードです。自己責任のもと、ご利用ください。

## 必要な環境

JDK 21 (Eclipse Temurin https://adoptium.net/)

MySQL 8 (https://dev.mysql.com/downloads/mysql/)

事前にご準備ください。

## DB環境作成

本プロジェクト直下にある `create-schema-spring.sql` を実行すると、アプリケーションから DBアクセスが可能です。

```
DROP DATABASE IF EXISTS spring;
CREATE DATABASE spring CHARACTER SET utf8mb4;
DROP USER IF EXISTS 'user'@'localhost';
CREATE USER 'user'@'localhost' IDENTIFIED BY 'P@ssw0rd';
GRANT ALL PRIVILEGES ON spring.* to 'user'@'localhost';
USE spring;
SHOW DATABASES;
```

user というDBユーザー、spring という名前のdetabase が、MySQL上に既にある場合、削除して再作成します。

ご注意ください。

## 含まれているサブプロジェクト

### 各章共通

・ 各サブプロジェクト直下の `GraphQL-Script` フォルダに、GraphQLを実行するquery/mutationスクリプトが入っています。

・ 各サブプロジェクト直下の `Diagram` フォルダに、各レイヤのクラス図、テーブルの関連を表わした物理ER図が入っています。

### graphql-intro-complete

「2. GraphQL入門」 、および「　3. Spring for GraphQLを利用した開発方法」で利用するサンプルアプリケーションです。

### graphql-data-loader

「3.1.11～18 <参考> N+1問題」のサンプルアプリケーションです。

### graphql-repository

「4. @GraphQlRepositoryによりSpring Data JPAリポジトリを直接リソースとして公開する」で利用するサンプルアプリケーションです。
