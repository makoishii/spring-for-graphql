package com.example.persistence.repository;

import com.example.persistence.entity.Employee;
import org.springframework.data.domain.Limit;
import org.springframework.data.domain.ScrollPosition;
import org.springframework.data.domain.Window;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

    /*
     * GraphQLアプリケーションにおけるページネーションはカーソルベースで行います。
     * Spring Dataはカーソルベースのページネーションに対応しており、
     * Repositoryインタフェースのメソッドの引数にScrollPositionを設定すると、
     * ユーザが指定したカーソル(位置・オフセット)からのデータを取得します。
     * また、引数にLimitも追加することで、取得するデータの上限数を指定できます。
     *
     * 戻り値はWindow型です。
     * データベースの検索結果のListと次ページのデータを取得するための開始位置などを内包しています。
     */
    Window<Employee> findAllBy(ScrollPosition position, Limit limit);
}
