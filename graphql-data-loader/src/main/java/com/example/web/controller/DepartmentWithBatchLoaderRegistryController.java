package com.example.web.controller;

import com.example.persistence.entity.Department;
import com.example.persistence.entity.Employee;
import com.example.service.DepartmentService;
import jakarta.annotation.PostConstruct;
import org.dataloader.DataLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.graphql.execution.BatchLoaderRegistry;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Mono;

import java.util.*;
import java.util.concurrent.CompletableFuture;

// @Controller
public class DepartmentWithBatchLoaderRegistryController {

    private final DepartmentService departmentService;

    private final BatchLoaderRegistry batchLoaderRegistry;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    /*
     * DIでBatchLoaderRegistryのBeanを取得する。
     */
    public DepartmentWithBatchLoaderRegistryController(DepartmentService departmentService, BatchLoaderRegistry batchLoaderRegistry) {
        this.departmentService = departmentService;
        this.batchLoaderRegistry = batchLoaderRegistry;
    }

    /*
     * 引数にDataLoaderを追加する。
     */
    @SchemaMapping
    public CompletableFuture<Department> department(Employee employee, DataLoader<Integer, Department> dataLoader) {
        logger.info("departmentメソッドが呼び出された");
        // DataLoaderのloadメソッドに引数で受け取ったEmployeeインスタンスのdepartmentIdを渡す。
        // loadメソッドの戻り値をそのまま返す。
        // 戻り値のFutureは非同期処理の結果を表すインタフェースで、即座に処理は実行されない。
        // （この場では検索対象の部署IDを蓄積しておく、とイメージするとわかりやすいと思います。）
        // EmployeeのListの要素数分departmentメソッドが呼び出されたら、
        // initメソッドで定義したバッチ処理が呼び出され、
        // 関連するDepartmentがSQLの実行一回で取得される。
        return dataLoader.load(employee.getDepartmentId());
    }

    /*
     * @PostConstructを付加したメソッドを作成すると
     * そのメソッドがBeanインスタンス生成時の初期化処理となる。
     */
    @PostConstruct
    public void init() {
        batchLoaderRegistry.forTypePair(Integer.class, Department.class)
                .registerMappedBatchLoader((departmentIds, batchLoaderEnvironment) -> {
                    // 部署を検索しその結果をMapに変換する
                    List<Department> departments = departmentService.findByIds(departmentIds);
                    Map<Integer, Department> departmentMap = new HashMap<>();
                    for (Department department : departments) {
                        departmentMap.put(department.getId(), department);
                    }
                    return Mono.just(departmentMap);
                });
    }

}
