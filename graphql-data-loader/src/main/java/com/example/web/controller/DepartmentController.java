package com.example.web.controller;

import com.example.persistence.entity.Department;
import com.example.persistence.entity.Employee;
import com.example.service.DepartmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.graphql.data.method.annotation.BatchMapping;
import org.springframework.stereotype.Controller;

import java.util.*;

@Controller
public class DepartmentController {

    private final DepartmentService departmentService;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public DepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @BatchMapping
    public Map<Employee, Department> department(List<Employee> employees) {
        logger.info("@BatchMapping:departmentメソッドが呼び出された");
        // 社員のListから検索対象の部署IDのSetを生成する。
        Set<Integer> targetDepartmentIds = new HashSet<>();
        for (Employee employee : employees) {
            targetDepartmentIds.add(employee.getDepartmentId());
        }
        // 部署を検索する。
        List<Department> departments = departmentService.findByIds(targetDepartmentIds);
        // 「key:Department.id、value:Department」のMap生成する。
        Map<Integer, Department> deptMap = new HashMap<>();
        for (Department department : departments) {
            deptMap.put(department.getId(), department);
        }
        // 「key:Employee、value:Department」のMap生成する。
        Map<Employee, Department> map = new HashMap<>();
        for (Employee employee : employees) {
            // employeeテーブルのdepartment_id列はNULLを許容しているので注意。
            if(employee.getDepartmentId() != null){
                map.put(employee, deptMap.get(employee.getDepartmentId()));
            }
            // department_id列はNULLの場合、mapにそのEmployeeは入らないが
            // 問題ない。呼び出し元ではList<Employee>を回して,結果を作成する
        }
        return map;
    }

}
