package com.example.web.controller;

import com.example.persistence.entity.Employee;
import com.example.service.EmployeeService;
import com.example.web.input.EmployeeInput;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.ScrollPosition;
import org.springframework.data.domain.Window;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.graphql.data.query.ScrollSubrange;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;

import java.util.OptionalInt;

@Controller
public class EmployeeController {

    private final EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    /*
     * ScrollSubrangeを引数に設定します。
     * 問い合わせを受け取るとフレームワークがQueryの引数で渡ってきた値を元に
     * ScrollSubrange型のインスタンスを生成し引数に代入します。
     *
     * 戻り値はWindow型です。
     * そうすることで、カーソルの開始位置・終了位置、前・次ページのデータの有無、検索結果をレスポンスできます。
     */
    @QueryMapping
    public Window<Employee> employees(ScrollSubrange subrange) {
        /*
         * 引数にはユーザが指定したカーソル(位置・オフセット)を表すScrollPositionと
         * 取得するデータの上限数を表すOptionalInt
         * が含まれるのでそれらを取り出してサービスクラスに渡し結果を取得します。
         */
        ScrollPosition position = subrange.position().orElse(ScrollPosition.offset());
        OptionalInt count = subrange.count();
        return employeeService.findAll(position, count);
    }

    @QueryMapping
    public Employee employee(@Argument Integer id) {
        return employeeService.findById(id);
    }

    @MutationMapping
    public Employee updateEmployee(@Argument Integer id, @Argument @Validated EmployeeInput employeeInput) {
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeInput, employee);
        employee.setId(id);
        employeeService.update(employee);
        return employee;
    }

    @MutationMapping
    public Employee insertEmployee(@Argument @Validated EmployeeInput employeeInput) {
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeInput, employee);
        employeeService.insert(employee);
        return employee;
    }

    @MutationMapping
    public String deleteEmployee(@Argument Integer id) {
        employeeService.delete(id);
        return "id: " + id + " の社員を削除しました";
    }

}
