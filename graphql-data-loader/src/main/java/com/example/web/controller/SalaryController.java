package com.example.web.controller;

import com.example.persistence.entity.Department;
import com.example.persistence.entity.Employee;
import com.example.persistence.entity.Salary;
import com.example.service.SalaryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.ScrollPosition;
import org.springframework.data.domain.Window;
import org.springframework.graphql.data.method.annotation.BatchMapping;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.graphql.data.query.ScrollSubrange;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.Map;
import java.util.OptionalInt;
import java.util.stream.Collectors;

@Controller
public class SalaryController {

    private final SalaryService salaryService;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public SalaryController(SalaryService salaryService) {
        this.salaryService = salaryService;
    }

    /*
     * Employeeの引数には取得した社員のデータが代入され実行される。
     */
    @BatchMapping
    public Map<Employee, List<Salary>> salaries(List<Employee> employees){
        logger.info("@BatchMapping:salariesメソッドが呼び出された");

        Iterable<Integer> employeeIds = employees.stream().map(employee -> employee.getId()).toList();
        List<Salary> list = salaryService.findByEmployeeIdIn(employeeIds);
        Map<Integer, List<Salary>> groupByEmployeeId = list.stream().collect(Collectors.groupingBy(salary -> salary.getEmployeeId()));
        Map<Employee, List<Salary>> result = employees.stream().collect(Collectors.toMap(employee -> employee, employee -> groupByEmployeeId.get(employee.getId())));
        return result;
    }

}
