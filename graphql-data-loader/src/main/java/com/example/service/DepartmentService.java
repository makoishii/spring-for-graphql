package com.example.service;

import com.example.persistence.entity.Department;

import java.util.List;
import java.util.Set;

public interface DepartmentService {

    Department findById(Integer id);

    List<Department> findByIds(Set<Integer> ids);
}
