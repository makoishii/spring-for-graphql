package com.example.service.impl;

import com.example.persistence.entity.Employee;
import com.example.persistence.repository.DepartmentRepository;
import com.example.persistence.repository.EmployeeRepository;
import com.example.persistence.repository.SalaryRepository;
import com.example.service.EmployeeService;
import org.springframework.data.domain.Limit;
import org.springframework.data.domain.ScrollPosition;
import org.springframework.data.domain.Window;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.OptionalInt;

@Service
@Transactional(readOnly = true)
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    private final DepartmentRepository departmentRepository;

    private final SalaryRepository salaryRepository;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository, DepartmentRepository departmentRepository, SalaryRepository salaryRepository) {
        this.employeeRepository = employeeRepository;
        this.departmentRepository = departmentRepository;
        this.salaryRepository = salaryRepository;
    }

    /*
     * ScrollPositionとOptionalIntの引数を追加します。
     * インタフェースも変更します。
     * どちらもコントローラーメソッドから渡されます。
     *
     * OptionalIntはint型を含む場合と含まない場合があるコンテナオブジェクトです。
     * 値を含む場合はisPresentメソッドの戻り値がtrueになるので、getAsIntメソッドで値を取り出し、
     * その値を取得するデータの上限数としています。
     * 値を含まない場合は上限なしで取得します。
     */
    @Override
    public Window<Employee> findAll(ScrollPosition position, OptionalInt count) {
        Limit limit = count.isPresent() ? Limit.of(count.getAsInt()) : Limit.unlimited();
        Window<Employee> result = employeeRepository.findAllBy(position, limit);
        return result;
    }

    @Override
    public Employee findById(Integer id) {
        return employeeRepository.findById(id).orElseThrow(() -> new RuntimeException("ID : %s の社員データは存在しません。".formatted(id)));
    }

    @Transactional
    @Override
    public Employee update(Employee employee) {
        // 更新対象のEmployeeが存在しない場合は例外をスローする。
        if (!employeeRepository.existsById(employee.getId())) {
            throw new RuntimeException("ID : %s の社員データは存在しません。".formatted(employee.getId()));
        }
        // 所属先の部署が存在しない場合は例外をスローする。
        if (!departmentRepository.existsById(employee.getDepartmentId())) {
            throw new RuntimeException("ID : %s の部署データは存在しません。".formatted(employee.getDepartmentId()));
        }
        // Employeeを保存(更新)する。
        return employeeRepository.save(employee);
    }

    @Transactional
    @Override
    public Employee insert(Employee employee) {
        // 既存のEmployeeを更新しようとしている場合は例外をスローする。
        if (employee.getId() != null && employeeRepository.existsById(employee.getId())) {
            throw new IllegalArgumentException("新しい社員を登録する場合はIDにnullもしくは存在しない値を設定してください。");
        }
        // 所属先の部署が存在しない場合は例外をスローする。
        if (!departmentRepository.existsById(employee.getDepartmentId())) {
            throw new RuntimeException("ID : %s の部署データは存在しません。".formatted(employee.getDepartmentId()));
        }
        // Employeeを保存(登録)する。
        return employeeRepository.save(employee);
    }

    @Transactional
    @Override
    public void delete(Integer id) {
        // 削除対象のEmployeeが存在しない場合は例外をスローする。
        if (!employeeRepository.existsById(id)) {
            throw new RuntimeException("ID : %s の社員データは存在しません。".formatted(id));
        }
        salaryRepository.deleteByEmployeeId(id);
        // Employeeを削除する。
        employeeRepository.deleteById(id);
    }
}
