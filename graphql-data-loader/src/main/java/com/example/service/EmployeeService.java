package com.example.service;

import com.example.persistence.entity.Employee;
import org.springframework.data.domain.ScrollPosition;
import org.springframework.data.domain.Window;

import java.util.OptionalInt;

public interface EmployeeService {

    Window<Employee> findAll(ScrollPosition position, OptionalInt count);

    Employee findById(Integer id);

    Employee update(Employee employee);

    Employee insert(Employee employee);

    void delete(Integer id);
}
