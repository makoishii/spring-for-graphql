package com.example.persistence.repository;

import com.example.persistence.entity.Employee;
import com.example.persistence.entity.QEmployee;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.repository.Repository;
import org.springframework.graphql.data.GraphQlRepository;

@GraphQlRepository
public interface EmployeeRepository extends Repository<Employee, Integer>,
        QuerydslPredicateExecutor<Employee>,
        QuerydslBinderCustomizer<QEmployee> {

    @Override
    default void customize(QuerydslBindings bindings, QEmployee root) {
        bindings.bind(root.name).first((path, value) -> path.contains(value));
    }
}
