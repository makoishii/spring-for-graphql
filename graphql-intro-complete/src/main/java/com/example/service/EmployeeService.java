package com.example.service;

import com.example.persistence.entity.Employee;

import java.util.List;

public interface EmployeeService {

    List<Employee> findAll();

    Employee findById(Integer id);

    Employee update(Employee employee);

    Employee insert(Employee employee);

    void delete(Integer id);

}
