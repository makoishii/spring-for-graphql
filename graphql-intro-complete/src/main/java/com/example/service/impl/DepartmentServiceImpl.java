package com.example.service.impl;

import com.example.persistence.entity.Department;
import com.example.persistence.repository.DepartmentRepository;
import com.example.service.DepartmentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class DepartmentServiceImpl implements DepartmentService {

    private final DepartmentRepository departmentRepository;

    public DepartmentServiceImpl(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }

    @Override
    public Department findById(Integer id) {
        return departmentRepository.findById(id).orElseThrow(() -> new RuntimeException());
    }
}
