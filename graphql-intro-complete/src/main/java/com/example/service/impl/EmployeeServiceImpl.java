package com.example.service.impl;

import com.example.persistence.entity.Employee;
import com.example.persistence.repository.DepartmentRepository;
import com.example.persistence.repository.EmployeeRepository;
import com.example.service.EmployeeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    private final DepartmentRepository departmentRepository;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository, DepartmentRepository departmentRepository) {
        this.employeeRepository = employeeRepository;
        this.departmentRepository = departmentRepository;
    }

    @Override
    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    @Override
    public Employee findById(Integer id) {
        return employeeRepository.findById(id).orElseThrow(() -> new RuntimeException());
    }

    @Transactional
    @Override
    public Employee update(Employee employee) {
        // 更新対象のEmployeeが存在しない場合は例外をスローする。
        if (!employeeRepository.existsById(employee.getId())) {
            throw new RuntimeException();
        }
        // 所属先の部署が存在しない場合は例外をスローする。
        if (!departmentRepository.existsById(employee.getDepartmentId())) {
            throw new RuntimeException();
        }
        // Employeeを保存(更新)する。
        return employeeRepository.save(employee);
    }

    @Transactional
    @Override
    public Employee insert(Employee employee) {
        // 既存のEmployeeを更新しようとしている場合は例外をスローする。
        if (employee.getId() != null && employeeRepository.existsById(employee.getId())) {
            throw new RuntimeException();
        }
        // 所属先の部署が存在しない場合は例外をスローする。
        if (!departmentRepository.existsById(employee.getDepartmentId())) {
            throw new RuntimeException();
        }
        // Employeeを保存(登録)する。
        return employeeRepository.save(employee);
    }

    @Transactional
    @Override
    public void delete(Integer id) {
        // 削除対象のEmployeeが存在しない場合は例外をスローする。
        if (!employeeRepository.existsById(id)) {
            throw new RuntimeException();
        }
        // Employeeを削除する。
        employeeRepository.deleteById(id);
    }

}
