package com.example.service;

import com.example.persistence.entity.Department;

public interface DepartmentService {

    Department findById(Integer id);
}
