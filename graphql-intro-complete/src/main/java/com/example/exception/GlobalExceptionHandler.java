package com.example.exception;

import graphql.GraphQLError;
import graphql.execution.ResultPath;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Path;
import org.springframework.graphql.data.method.annotation.GraphQlExceptionHandler;
import org.springframework.graphql.execution.ErrorType;
import org.springframework.web.bind.annotation.ControllerAdvice;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@ControllerAdvice
public class GlobalExceptionHandler {

    /*
     * 制約違反発生時の例外処理を実行する。
     *
     * 実行時にConstraintViolationExceptionの例外がスローされると
     * 引数に該当の例外インスタンスが渡されメソッドが自動的に実行される。
     *
     * 戻り値はGraphQLErrorを要素とするListとする。
     * GraphQLErrorはGraphQLでのエラーを表すインタフェースで、
     * 例外ハンドラーメソッドの戻り値として返すと、
     * レスポンスされるメッセージが自動的に構築される。
     */
    @GraphQlExceptionHandler
    public List<GraphQLError> handleInvalidationError(ConstraintViolationException exception) {
        List<GraphQLError> errors = new ArrayList<>();
        // getConstraintViolationsメソッドで制約違反の内容を全て取出す。
        for (ConstraintViolation constraintViolation : exception.getConstraintViolations()) {
            // 制約違反が発生したプロパティ名を取得する。
            // getPropertyPathメソッドの戻り値のjakarta.validation.Pathは、
            // java.util.Iteratorを継承したイテレーターである。
            // インプットクラスを入れ子にした場合でも制約違反が発生したプロパティ名を取得できる仕組みとなっている。
            Path targetPath = constraintViolation.getPropertyPath();
            Iterator<Path.Node> iterator = targetPath.iterator();
            List<String> paths = new ArrayList<>();
            while (iterator.hasNext()) {
                Path.Node node = iterator.next();
                paths.add(node.getName());
            }
            // staticなnewErrorメソッドでGraphQLErrorのインスタンスを作成できる。
            GraphQLError error = GraphQLError.newError()
                    // errorTypeメソッドで発生したエラーの分類を設定する。
                    .errorType(ErrorType.BAD_REQUEST)
                    // messageメソッドで制約違反が発生したインプットクラスのプロパティ名と
                    // 制約違反内容を整形しエラーメッセージを設定する。
                    .message(constraintViolation.getMessage())
                    // 制約違反が発生したプロパティ名を設定する。
                    .path(ResultPath.fromList(paths))
                    .build();
            errors.add(error);
        }
        return errors;
    }

}
