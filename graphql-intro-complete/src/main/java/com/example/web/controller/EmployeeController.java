package com.example.web.controller;

import com.example.persistence.entity.Employee;
import com.example.service.EmployeeService;
import com.example.web.input.EmployeeInput;
import org.springframework.beans.BeanUtils;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Controller
public class EmployeeController {

    private final EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @QueryMapping
    public List<Employee> employees() {
        return employeeService.findAll();
    }

    @QueryMapping
    public Employee employee(@Argument Integer id) {
        return employeeService.findById(id);
    }

    /*
     * Mutationに対応したコントローラーメソッドには@MutationMappingを付加する
     * メソッド名をGraphQLのSchemaに定義したMutationの名前と完全に一致させることで
     * updateEmployeeのMutationのリクエストを受信すると自動的に呼び出される
     *
     * メソッドの引数に@Argumentを付加することでMutationのリクエストで送信されてきたパラメータ受け取れる
     * パラメータ名と引数名を一致させる必要があるので注意
     */
    @MutationMapping
    public Employee updateEmployee(@Argument Integer id, @Argument @Validated EmployeeInput employeeInput) {
        // クライアントから送信されてきたパラメータを元に新しいエンティティを生成する
        Employee employee = new Employee();
        // Spring FrameworkのBeanUtilsを使うことで
        // Javaインスタンスがフィールドに保持している値を同名のフィールドにコピーすることができる。
        // 第一引数がコピー元のインスタンス、第二引数がコピー先のインスタンス
        // サンプルではクライアントから送信されてきたパラメータをエンティティにコピーしている
        BeanUtils.copyProperties(employeeInput, employee);
        // 更新対象の主キーはidパラメータとして送信されてくるので別途設定する
        employee.setId(id);
        // 更新処理を呼び出す
        employeeService.update(employee);
        // クライアントには更新後のデータをレスポンスする
        return employee;
    }

    /*
     * updateEmployeeメソッド同様@MutationMappingを付加する
     * 登録データを受け取るため@Argumentを付加した引数を設定する
     */
    @MutationMapping
    public Employee insertEmployee(@Argument @Validated EmployeeInput employeeInput) {
        // idフィールドがnullのエンティティを生成する
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeInput, employee);
        // 登録処理を呼び出す
        // employeeテーブルのid列はAUTO_INCREMENTなので自動採番される
        // 自動採番された値はエンティティクラスのフィールドに付加しアノテーションの設定によって
        // idフィールドに自動的に代入される
        employeeService.insert(employee);
        // クライアントには登録後のデータをレスポンスする
        return employee;
    }

    /*
     * 二つのメソッド同様に@MutationMappingを付加する
     * @Argumentを付加した引数を設定し削除対象の主キーを受け取る
     */
    @MutationMapping
    public String deleteEmployee(@Argument Integer id) {
        employeeService.delete(id);
        return "id: " + id + " の社員を削除しました";
    }

}
