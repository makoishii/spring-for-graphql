package com.example.web.controller;

import com.example.persistence.entity.Department;
import com.example.persistence.entity.Employee;
import com.example.service.DepartmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.stereotype.Controller;

@Controller
public class DepartmentController {

    private final DepartmentService departmentService;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public DepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @SchemaMapping
    public Department department(Employee employee) {
        logger.info("departmentメソッドが呼び出された");
        logger.info(employee.toString());
        return departmentService.findById(employee.getDepartmentId());
    }
}
