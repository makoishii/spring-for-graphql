'use strict';

const tableElement = document.getElementById('table');

/*
 * GraphQLアプリケーションへの問い合わせは "/graphql" というパスに
 * POSTでリクエストを送信する
 */
fetch("/graphql", {
    method: "POST",
    headers: {
        'Content-Type': 'application/json'
    },
    /*
     * "query"プロパティを持つJSONを送信する
     * "query"プロパティにはGraphiQLで作成したQueryを文字列として持たせる
     */
    body: JSON.stringify({
        query: "{ employees {id name joinedDate departmentId email birthDay } }"
    }),
})
    .then(r => r.json())
    .then(result => {
        // 取得したデータを使いtable要素に行を追加する
        const employees = result.data.employees;
        let trElements = '';
        for (const employee of employees) {
            let tdElements = '';
            for (const field in employee) {
                const value = employee[field];
                tdElements += `<td>${value}</td>`
            }
            trElements += `<tr>${tdElements}</tr>`
        }
        tableElement.innerHTML += trElements
    });