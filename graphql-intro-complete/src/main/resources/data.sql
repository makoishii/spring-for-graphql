INSERT INTO department VALUES(10, '営業部');
INSERT INTO department VALUES(20, '開発部');
INSERT INTO department VALUES(30, '管理部');

INSERT INTO employee VALUES(101, '山田太郎', '2010-04-01', 10, 'yamada@example.com', '1986-05-11');
INSERT INTO employee VALUES(102, '鈴木次郎', '2010-05-01', 20, 'suzuki@example.com', '1989-06-19');
INSERT INTO employee VALUES(103, '高田純平', '2010-04-01', 30, 'takada@example.com', '1979-12-01');
INSERT INTO employee VALUES(104, '大山里美', '2010-05-01', 20, 'oyama@example.com', '1990-01-11');
INSERT INTO employee VALUES(105, '橋本哲也', '2011-04-01', 10, 'hashimoto@example.com', '1989-07-17');
INSERT INTO employee VALUES(106, '本山百合子', '2011-05-01', 20, 'motoyama@example.com', '1982-09-13');
INSERT INTO employee VALUES(107, '中田かすみ', '2011-04-01', 30, 'nakata@example.com', '1981-02-22');
INSERT INTO employee VALUES(108, '田中亮太', '2011-05-01', 10, 'tanaka@example.com', '1986-04-11');
INSERT INTO employee VALUES(109, '長谷川宏', '2012-04-01', 20, 'hasegawa@example.com', '1989-11-29');
